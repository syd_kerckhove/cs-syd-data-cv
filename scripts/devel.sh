#!/usr/bin/env bash

set -x
file="$1"
evince "$(basename "$file" .tex).pdf" 2>&1 >/dev/null &
watchexec --watch manual --exts tex,json -- "cvgen && latexmk -pdf $file"

