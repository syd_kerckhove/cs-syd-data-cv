#!/usr/bin/env bash

set -e
set -x

cvgen
cd auto/gen
pwd

mkpdf () {
  file="$1"
  latexmk -pdf "${file}.tex"
}

mkpdf "failcv"
mkpdf "for-recruiters"
mkpdf "for-engineers"
mkpdf "cv"
mkpdf "speaking"
