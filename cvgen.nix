{ pkgs ? import <nixpkgs> { config.allowUnfree = true; }
}:
let
  cvgenRepo = pkgs.fetchgit {
      url = "https://gitlab.com/Norfair/cvgen";
      rev = "e30f09104aefcb870b3a6aaee8dfdb022ffcef5d";
      sha256 = "sha256:08fasyb4f91xcwp1hs2y2ixlpp0splc354gb25wybrwi6riqxr8q";
    };

  cvgen = with pkgs.haskell.lib;
    disableLibraryProfiling (
        pkgs.haskellPackages.callCabal2nix "cvgen" ( cvgenRepo ) {}
    );
in cvgen
