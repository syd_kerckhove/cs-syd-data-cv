{ pkgs }:

let
  gitignoreSourceRepo = pkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore";
    rev = "dcd0b0e878009ef7b1425f8ca3e5b883f12459d8";
    sha256 = "sha256:0ni9hl6j4l94mfwsdmrg5l0ffibfgd8amspda7vc5fb3wm8blxpy";
  };
  inherit (import gitignoreSourceRepo { inherit (pkgs) lib; }) gitignoreSource;
in gitignoreSource

