{ pkgs ? import (import ./nixpkgs.nix) { config.allowUnfree = true; }
}:

with pkgs;

let
  cvgen = import ./cvgen.nix { inherit pkgs; };
  gitignoreSource = import ./gitignore.nix { inherit pkgs; };
  latexenv = import ./latexenv.nix { inherit pkgs; };

in pkgs.mkShell {
  name = "cv";
  buildInputs = [
    bash
    cvgen
    latexenv
    watchexec
    evince
  ];
  src = gitignoreSource ./.;
  shellHook = ''
    function build () {
      cvgen
      pushd auto/gen
      latexmk -pdf failcv.tex
      latexmk -pdf for-recruiters.tex
      latexmk -pdf for-engineers.tex
      latexmk -pdf cv.tex
      popd
    }
  '';
}

